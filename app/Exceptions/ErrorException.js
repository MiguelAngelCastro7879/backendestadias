'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

class ErrorException extends LogicalException {
  
  /**
   *
   * @param {Response} ctx.response
   */
   handle (error, {response}, modelo) {
    // console.log(error)}
    switch(modelo){
      case "usuario":
        switch (error.code) {
          case 'E_PASSWORD_MISMATCH':
            return response.badRequest({error:'Contraseña incorrecta'})
            break;
          case 'E_USER_NOT_FOUND':
            return response.notFound({error:'Usuario no encontrado'})
            break;
          case 'E_MISSING_DATABASE_ROW':
            return response.notFound({error:'Usuario no encontrado'})
            break;
          case 'E_INVALID_JWT_TOKEN':
            return response.unauthorized({error:'Token invalido'})
            break;
          case 'ER_DUP_ENTRY':
            return response.conflict({error:'El correo electronico ya está registrado'})
            break;
          case 'E_JWT_TOKEN_EXPIRED':
            return response.conflict({error:'El token ha expirado'})
            break;
          case 'E_INVALID_JWT_REFRESH_TOKEN':
            console.log("fallo el refresh")
            return response.status(403).send({error:'No se proporcionó un refresh token'})
            break;
          default:
            console.log(error)
            return response.status(400).send({error:error.code})
            break;
        }
        break;



      case "role":
        switch (error.code) {
          case '':
            return response.status(500)
            break;
          case 'E_MISSING_DATABASE_ROW':
            return response.notFound({error:'Rol no encontrado'})
            break;
          default:
            console.log(error)
            return response.status(400).send({error:error.code})
            break;
        }
        break



      case "view":
        switch (error.code) {
          case '':
            return response.status(500)
            break;
          case 'E_MISSING_DATABASE_ROW':
            return response.notFound({error:'Vista no encontrada'})
            break;
          case 'ER_NO_REFERENCED_ROW_2':
            return response.notFound({error:'Se ingreso una vista no existente'})
            break;
          default:
            console.log(error)
            return response.status(400).send({error:error.code})
            break;
        }
        break

        
      case "category":
        switch (error.code) {
          case '':
            return response.status(500)
            break;
          case 'E_MISSING_DATABASE_ROW':
            return response.notFound({error:'Categoria no encontrada'})
            break;
          default:
            console.log(error)
            return response.status(400).send({error:error.code})
            break;
        }
        break
    }
  }
}

module.exports = ErrorException
