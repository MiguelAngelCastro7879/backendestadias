'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Role extends Model {
    
  static get table () {
    return 'roles'
  }

  static get crear(){
    return ['name']
  }
  
  static get actualizar(){
    return ['name']
  }
  
  vistas() {
    return this.belongsToMany('App/Models/Support/Vista')
    .pivotTable('vistas_roles')
  }
  
  categorias () {
    return this.manyThrough('App/Models/Support/Vista', 'categoria')
  }

}

module.exports = Role
