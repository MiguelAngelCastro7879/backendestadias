'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Categoria extends Model {
    
  static get table () {
    return 'categorias'
  }

  static get crear (){
    return [ 
      'nombre',
      'icono',
      'nivel',
    ]
  }
  static get actualizar(){
    return [ 
      'nombre',
      'icono',
      'nivel',
    ]
  }

  
  vistas () {
    return this.hasMany('App/Models/Support/Vista')
  }
}

module.exports = Categoria
