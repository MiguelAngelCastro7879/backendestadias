'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Vista extends Model {
    
  static get table () {
    return 'vistas'
  }

  static get crear (){
    return [ 
      'nombre',
      'icono',
      'nivel',
      'ruta',
      'categoria_id',
    ]
  }
  static get actualizar(){
    return [ 
      'nombre',
      'icono',
      'nivel',
      'ruta',
      'categoria_id',
    ]
  }
  
  roles() {
    return this.belongsToMany('App/Models/Support/Role')
    .pivotTable('vistas_roles')
  }
  categoria () {
    return this.belongsTo('App/Models/Support/Categoria')
  }

}

module.exports = Vista
