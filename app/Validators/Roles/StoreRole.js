'use strict'

class StoreRole {
  
  get validateAll () {
    return true
  }

  async fails (errorMessages) {
    return this.ctx.response.badRequest({errores:errorMessages})
  }
  
  get rules () {
    return {
      name: "required",
    }
  }
  
  get messages () {
    return {
      'name.required': 'You must provide a name.',
    }
  }
}

module.exports = StoreRole
