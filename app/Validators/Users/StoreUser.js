'use strict'

class StoreUser {
  
  get validateAll () {
    return true
  }

  get rules () {
    return {
      username: "required",
      email: "required|email",
      password: "required",
      // role_id: "required",
    }
  }
  
  get messages () {
    return {
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email address.',
      // 'email.unique': 'This email is already registered.',
      'password.required': 'You must provide a password',
      'username.required': 'You must provide a username.',
      // 'role_id.required': 'You must provide a role id.',
    }
  }
  
  async fails (errorMessages) {
    return this.ctx.response.badRequest({errores:errorMessages})
  }
}

module.exports = StoreUser
