'use strict'

class StoreView {
  
  get validateAll () {
    return true
  }

  async fails (errorMessages) {
    return this.ctx.response.badRequest({errores:errorMessages})
  }

  get rules () {
    return {
      nombre: "required",
      icono: "required",
      nivel: "required",
      ruta: "required",
    }
  }
  
  get messages () {
    return {
      'nombre.required': 'You must provide a name.',
      'icono.required': 'You must provide a icon.',
      'nivel.required': 'You must provide a level.',
      'ruta.required': 'You must provide a route.',
    }
  }
}

module.exports = StoreView
