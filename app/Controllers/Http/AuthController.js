'use strict'
const User = use('App/Models/User')

const ErrorException = require('../../Exceptions/ErrorException')
const errores = new ErrorException()

const Role = use('App/Models/Support/Role')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')


class AuthController {
    
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async  login ({auth, request, response }) {
    const email = request.input("email")
    const password = request.input("password");
    try {
      const user = await User.findByOrFail('email', request.input("email"))
      if(user.status){
        //el tiempo que toma el expiresIn es en milisegundos, en este caso toma 1 hora para expirar el token
        // const token = await auth.withRefreshToken().attempt(email, password,{},{expiresIn:'3600000'})
        const token = await auth.withRefreshToken().attempt(email, password)
        return response.ok({ mensaje:"Sesion iniciada correctamente","access_token": token})
      }
      else{
        return response.badRequest({ error:"Usuario no encontrado"})
      }
    }
    catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }

  async  register ({request, response }) {
    try {
      const usuario = await User.create(request.only(User.crear))
      usuario.role_id = (await Role.findByOrFail("name", "user")).id
      usuario.save()
      return response.ok({
        user:usuario,
        mensaje:'Usuario creado correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async  usuario ({auth, request, response }) {
    try {
      const usuario = await auth.getUser()
      response.ok({
        usuario: usuario
      })
    } catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async refreshToken ({auth, request, response }) {
    try {
      const refreshToken = request.input('refresh_token')
      const token = await auth.generateForRefreshToken(refreshToken, true)
      return response.ok({"access_token": token})
    }
    catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }
  
  // /**
  //  * Update user details.
  //  * PUT or PATCH users/:id
  //  *
  //  * @param {object} ctx
  //  * @param {Request} ctx.request
  //  * @param {Response} ctx.response
  //  */
  //  async changePassword({ auth, request, response }) {
  //   try {
  //     const pass = await auth.getUser()
  //     const user = await User.find(pass.id)
  //     if(await Hash.verify(request.input('password'), pass.password)){
  //       user.password = request.input("new_password")
  //       await user.save() 
  //       return response.ok({mensaje:"Contraseña modificada correctamente"})
  //     }
  //     return response.unauthorized({error:"Contraseña incorrecta"})
  //   } catch (error) {
  //     errores.handle(error, {response}, "usuario")
  //   }
  // }
}

module.exports = AuthController
