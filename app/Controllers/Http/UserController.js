'use strict'


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use('App/Models/User')
const { validate } = use('Validator')

const Role = use('App/Models/Support/Role')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

const ErrorException = require('../../Exceptions/ErrorException')
const errores = new ErrorException()

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const usuarios= await User.all()
    return response.ok({
      users:usuarios
    })

  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try {
      const usuario = await User.create(request.only(User.crear))
      return response.ok({
        user:usuario,
        mensaje:'Usuario creado correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response }) {
    try {
      const usuario = await User.findOrFail(params.id)
      return response.ok({
        user:usuario
      })
    } catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }
  
  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    try {
      const usuario = await User.findOrFail(params.id)
      await usuario.merge(request.only(User.actualizar))
      await usuario.save()
      return response.ok({
        user:usuario,
        mensaje:'Usuario actualizado correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }
  

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const usuario = await User.findOrFail(params.id)
      usuario.status = !usuario.status
      usuario.save()
      let mensaje
      if(usuario.status){mensaje = 'Usuario activado correctamente'}
      else{mensaje = 'Usuario desactivado correctamente'}
      return response.ok({
        user:usuario,
        mensaje:mensaje
      })
    } catch (error) {
      errores.handle(error, {response}, "usuario")
    }
  }
  
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
   async usersWithRoles ({ response }) {
    const usuarios= await User.query().with('role').fetch()
    return response.ok({
      users:usuarios
    })

  }
}

module.exports = UserController
