'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Role = use('App/Models/Support/Role')
const { validate } = use('Validator')

// const RoleException = require('../../Exceptions/RoleException')
// const errores = new RoleException()


const ErrorException = require('../../../Exceptions/ErrorException')
const errores = new ErrorException()

/**
 * Resourceful controller for interacting with roles
 */
class RoleController { 
  /**
   * Show a list of all roles.
   * GET roles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const roles= await Role.all()
    return response.status(200).send({
      roles:roles
    })
  }

  /**
   * Create/save a new role.
   * POST roles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try {
      const role = await Role.create(request.only(Role.crear))
      return response.status(200).send({
        role:role,
        mensaje:'Rol creado correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "role")
    }
  }

  /**
   * Display a single role.
   * GET roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response }) {
    try {
      const role = await Role.findOrFail(params.id)
      return response.status(200).send({
        role:role
      })
    } catch (error) {
      errores.handle(error, {response}, "role")
    }
  }

  /**
   * Update role details.
   * PUT or PATCH roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    
    try {
      const role = await Role.findOrFail(params.id)
      await role.merge(request.only(Role.actualizar))
      await role.save()
      return response.ok({
        role:role,
        mensaje:'Rol actualizado correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "role")
    }
  }

  /**
   * Delete a role with id.
   * DELETE roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    
    try {
      const role = await Role.findOrFail(params.id)
      role.status = !role.status
      role.save()
      
      let mensaje
      if(role.status){mensaje = 'Rol activado correctamente'}
      else{mensaje = 'Rol desactivado correctamente'}
      return response.ok({
        role:role,
        mensaje:mensaje
      })

    } catch (error) {
      errores.handle(error, {response}, "role")
    }
  }

  
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async setViews ({ request, response }) {
    try {
      const role = await Role.findOrFail(request.input('role'))
      const vistas = request.input("vistas")
      // await role.vistas().detach()
      // await role.vistas().attach(vistas)
      await role.vistas().sync(vistas)

      const rol = await Role.query().with('vistas').where('id',request.input('role')).fetch()
      
      return response.ok({
        role:rol,
        mensaje:'Vistas asignadas correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "view")
    }
  }
}

module.exports = RoleController
