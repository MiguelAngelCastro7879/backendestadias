'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const Categoria = use('App/Models/Support/Categoria')

const ErrorException = require('../../../Exceptions/ErrorException')
const errores = new ErrorException()
/**
 * Resourceful controller for interacting with categorias
 */
class CategoriaController {
  /**
   * Show a list of all categorias.
   * GET categorias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const categoria= await Categoria.all()
    return response.ok({
      categories:categoria
    })

  }

  /**
   * Create/save a new categoria.
   * POST categorias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try {
      const categoria = await Categoria.create(request.only(Categoria.crear))
      return response.ok({
        category:categoria,
        mensaje:'Categoria creada correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "category")
    }
  }

  /**
   * Display a single categoria.
   * GET categorias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const categoria = await Categoria.findOrFail(params.id)
      return response.ok({
        category:categoria
      })
    } catch (error) {
      errores.handle(error, {response}, "category")
    }
  }

  /**
   * Update categoria details.
   * PUT or PATCH categorias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    try {
      const categoria = await Categoria.findOrFail(params.id)
      await categoria.merge(request.only(Categoria.actualizar))
      await categoria.save()
      return response.ok({
        category:categoria,
        mensaje:'Categoria actualizada correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "category")
    }
  }

  /**
   * Delete a categoria with id.
   * DELETE categorias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const categoria = await Categoria.findOrFail(params.id)
      categoria.status = !categoria.status
      categoria.save()
      
      let mensaje
      if(categoria.status){mensaje = 'Categoria activada correctamente'}
      else{mensaje = 'Categoria desactivada correctamente'}
      return response.ok({
        category:categoria,
        mensaje:mensaje
      })
      return response.ok({
        mensaje:'Categoria eliminada correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "category")
    }
  }
  /**
   * Delete a role with id.
   * DELETE roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getViewsByRole({params, response, request, auth}){
    try{
      const usuario = await auth.getUser()
      const hasData = await Categoria
      .query()
      .whereHas('vistas', (query) => {
        query.whereHas('roles', (builder) => {
          builder.where('role_id', usuario.role_id)
        })
      }).with('vistas', (query) => {
        query.whereHas('roles', (builder) => {
          builder.where('role_id', usuario.role_id)
          builder.where('vistas.status', true)
        })
      })
      .where('status', true)
      .fetch()
      // console.log(hasData)
      return response.ok({
        categories:hasData
      })
    }catch(error){
      errores.handle(error, {response}, "category")
    }
  }
  
  
  async showViewsByRole({params, response, request, auth}){
    try{
      const hasData = await Categoria
      .query()
      .whereHas('vistas', (query) => {
        query.whereHas('roles', (builder) => {
          builder.where('role_id', params.id)
        })
      }).with('vistas', (query) => {
        query.whereHas('roles', (builder) => {
          builder.where('role_id', params.id)
          builder.where('vistas.status', true)
        })
      })
      .where('status', true)
      .fetch()
      // console.log(hasData)
      return response.ok({
        categories:hasData
      })
    }catch(error){
      errores.handle(error, {response}, "category")
    }
  }

  async getViews({response}){
    try{
      const hasData = await Categoria
      .query()
      .whereHas('vistas')
      // .with('vistas', (query) => {
      //   query.whereHas('roles', (builder) => {
      //     query.where('vistas.status', true)
      //   })
      // })
      .with('vistas', (query) => {
          query.where('vistas.status', true)
      })
      // .where('status', true)
      .fetch()
      return response.ok({
        categories:hasData
      })
    }catch(error){
      errores.handle(error, {response}, "category")
    }
  }
  
}

module.exports = CategoriaController
