'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Vista = use('App/Models/Support/Vista')

// const ViewException = require('../../Exceptions/ViewException')
// const errores = new ViewException()

const ErrorException = require('../../../Exceptions/ErrorException')
const errores = new ErrorException()

const Event = use('Event')

/**
 * Resourceful controller for interacting with vistas
 */
class VistaController {
  /**
   * Show a list of all vistas.
   * GET vistas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */   
  async index ({ request, response, view }) {
    const vistas= await Vista.all()
    return response.status(200).send({
      vistas:vistas
    })
  }
  
  /**
   * Create/save a new vista.
   * POST vistas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try {
      const vista = await Vista.create(request.only(Vista.crear))
      
      // Event.fire('new::vista', vista)
      return response.status(200).send({
        vista:vista,
        mensaje:'Vista creada correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "view")
    }
  }

  /**
   * Display a single vista.
   * GET vistas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const vista = await Vista.findOrFail(params.id)
      return response.status(200).send({
        vista:vista
      })
    } catch (error) {
      errores.handle(error, {response}, "view")
    }
  }

  /**
   * Update vista details.
   * PUT or PATCH vistas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    try {
      const vista = await Vista.findOrFail(params.id)
      await vista.merge(request.only(Vista.actualizar))
      await vista.save()
      return response.ok({
        vista:vista,
        mensaje:'Vista actualizada correctamente'
      })
    } catch (error) {
      errores.handle(error, {response}, "view")
    }
  }

  /**
   * Delete a vista with id.
   * DELETE vistas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const vista = await Vista.findOrFail(params.id)
      vista.status = !vista.status
      vista.save() 
      
      let mensaje
      if(vista.status){mensaje = 'Vista activada correctamente'}
      else{mensaje = 'Vista desactivada correctamente'}
      return response.ok({
        vista:vista,
        mensaje:mensaje
      })
    } catch (error) {
      errores.handle(error, {response}, "view")
    }
  }

  
  async viewsWithCategories ({ response }) {
    const vistas= await Vista.query().with('categoria').fetch()
    return response.status(200).send({
      vistas:vistas
    })
  }
  
}

module.exports = VistaController
