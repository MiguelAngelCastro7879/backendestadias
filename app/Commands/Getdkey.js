'use strict'

const { Command } = require('@adonisjs/ace')

/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

class Getdkey extends Command {
  static get signature () {
    return 'getdkey'
  }

  static get description () {
    return 'Tell something helpful about this command'
  }

  async handle (args, options) {
    this.info('Dummy implementation for getdkey command')
    console.log(Env.get('DEBUGGER_KEY'))
    
  }
}

module.exports = Getdkey
