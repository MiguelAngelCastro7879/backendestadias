'use strict'

const { Command } = require('@adonisjs/ace')


/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

class Debugger extends Command {
  static get signature () {
    return 'key:generate:debugger'
  }

  static get description () {
    return 'Sirve para crear un app key para el roll debbuger'
  }

  async handle (args, options) {
    this.info('Dummy implementation for debugger command')
    const Config = use('Config')

    // Config.set('app.debuggerKey', Math.random().toString(36).substring(32))
    // console.log(Config.get('app.debuggerKey'))
    // console.log(Math.random().toString(36).substring(32))

    
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result1= '';
    const charactersLength = characters.length;
    for ( let i = 0; i < 32; i++ ) {
        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    
    Env.set('DEBUGGER_KEY', result1)
    // console.log("esta es la llave",Env.get('DEBUGGER_KEY'), Env.getEnvPath())
    // console.log("resultado", result1)
    // console.log(result1, result1.length)

  }
}

module.exports = Debugger
