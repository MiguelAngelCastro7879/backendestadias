'use strict'

/*
|--------------------------------------------------------------------------
| CategorySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const Categoria = use('App/Models/Support/Categoria')

const categories = require('./Files/Categories/Categories')

class CategorySeeder {
  async run () {
  }
  create(){
    
    Categoria.createMany(categories)

  }
}

module.exports = CategorySeeder
