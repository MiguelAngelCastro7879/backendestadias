'use strict'

/*
|--------------------------------------------------------------------------
| ViewSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const Vista = use('App/Models/Support/Vista')

const views= require('./Files/Views/Views')
class ViewSeeder {
  async run () {
  }
  create(){
    Vista.createMany(views)
  }
}

module.exports = ViewSeeder
