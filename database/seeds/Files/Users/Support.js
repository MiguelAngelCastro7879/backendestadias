const pass = "123456789"
const supports = [
    {
        "username":"@support1",
        "email":"support1@gmail.com",
        "password":pass
    },{
        "username":"@support2",
        "email":"support2@gmail.com",
        "password":pass
    },{
        "username":"@support3",
        "email":"support3@gmail.com",
        "password":pass
    },{
        "username":"@support4",
        "email":"support4@gmail.com",
        "password":pass
    },{
        "username":"@support5",
        "email":"support5@gmail.com",
        "password":pass
    }
]

module.exports = supports