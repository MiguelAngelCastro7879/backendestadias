const views = [
    {
        "nombre": "Vistas",
        "icono": "video_label",
        "nivel": "1",
        "ruta": "views",
        "categoria_id": 2
      },
      {
        "nombre": "Usuarios",
        "icono": "contacts",
        "nivel": "2",
        "ruta": "users",
        "categoria_id": 4
      },
      {
        "nombre": "Categorias",
        "icono": "subscriptions",
        "nivel": "2",
        "ruta": "categories",
        "categoria_id": 2
      },
      {
        "nombre": "Roles",
        "icono": "manage_accounts",
        "nivel": "1",
        "ruta": "roles",
        "categoria_id": 4
      },
      {
        "nombre": "Operadores",
        "icono": "fact_check",
        "nivel": "1",
        "ruta": "operadores",
        "categoria_id": 5
      },
      {
        "nombre": "Asignar vistas",
        "icono": "dashboard",
        "nivel": "1",
        "ruta": "assign/views",
        "categoria_id": 2
      }
]

module.exports = views