const categories = [
    {
        "nombre": " Categoria",
        "icono": "subscriptions",
        "nivel": "1",
    },
    {
        "nombre": "Usuarios",
        "icono": "contacts",
        "nivel": "1",
    },
    {
        "nombre": "Reportes",
        "icono": "article",
        "nivel": "1",
    }
]

module.exports = categories