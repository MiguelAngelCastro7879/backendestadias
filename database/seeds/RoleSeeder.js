'use strict'

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const Role = use('App/Models/Support/Role')

const roles = require('./Files/Roles/Roles')
class RoleSeeder {
  async run () {
  }
  create(){
    Role.createMany(roles)
  }
}

module.exports = RoleSeeder
