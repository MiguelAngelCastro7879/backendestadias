'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const UserSeeder = require('./UserSeeder')
const ViewSeeder = require('./ViewSeeder')
const RoleSeeder = require('./RoleSeeder')
const CategorySeeder = require('./CategorySeeder')



const userSeeder = new UserSeeder()
const viewSeeder = new ViewSeeder()
const roleSeeder = new RoleSeeder()
const categorySeeder = new CategorySeeder()

class DatabaseSeeder {
  async run () {
    roleSeeder.crear()
    categorySeeder.crear()
    userSeeder.crear()
    viewSeeder.crear()
  }
}
//adonis seed --files DatabaseSeeder.js --force
module.exports = DatabaseSeeder
