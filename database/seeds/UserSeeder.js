'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
const admin = require('./Files/Users/Admin')
const supports = require('./Files/Users/Support')
class UserSeeder {
  async run() {
  }
  crear() {
    // console.log(admin)
    User.createMany(admin)
    User.createMany(supports)
  }
}

module.exports = UserSeeder
