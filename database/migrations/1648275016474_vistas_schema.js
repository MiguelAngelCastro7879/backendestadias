'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VistasSchema extends Schema {
  up () {
    this.create('vistas', (table) => {
      table.increments()
      table.string('nombre')
      table.string('icono')
      table.string('nivel')
      table.string('ruta')
      table.boolean('status').notNullable().defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('vistas')
  }
}

module.exports = VistasSchema
