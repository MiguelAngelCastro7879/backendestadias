'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VistasSchema extends Schema {
  up () {
    this.table('vistas', (table) => {
      table.integer('categoria_id').unsigned().references('id').inTable('categorias')
    })
  }

  down () {
    this.table('vistas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = VistasSchema
