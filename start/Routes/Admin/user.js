/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.resource('users', 'UserController').apiOnly()
Route.get('get/users', 'UserController.usersWithRoles')  
Route.get('get/user', 'AuthController.usuario')