/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.resource('views', 'Support/VistaController').apiOnly()
.validator(new Map([
    [['views.store'], ['Views/StoreView']]
  ]))
Route.get('get/views', 'Support/VistaController.viewsWithCategories')