/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.resource('categories', 'Support/CategoriaController').apiOnly()
.validator(new Map([
  [['categories.store'], ['Categories/StoreCategory']]
]))
Route.get('role/views', 'Support/CategoriaController.getViewsByRole')
Route.get('role/views/:id', 'Support/CategoriaController.showViewsByRole')
Route.get('category/views', 'Support/CategoriaController.getViews')