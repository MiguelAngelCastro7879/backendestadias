/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.resource('roles', 'Support/RoleController').apiOnly()
.validator(new Map([
    [['roles.store'], ['Roles/StoreRole']]
  ]))
Route.post('set/views', 'Support/RoleController.setViews')