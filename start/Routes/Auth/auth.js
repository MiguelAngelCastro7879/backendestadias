/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('register', 'AuthController.register').validator('Users/StoreUser')
Route.post('login', 'AuthController.login')
Route.post('change/password', 'AuthController.changePassword')
Route.post('refresh/token', 'AuthController.refreshToken')