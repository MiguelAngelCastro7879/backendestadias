'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.get('/', ({request}) => {
//   return {mensaje:'Hola mundo en JSON'}
// })

// Route.get('/hola', ({auth}) => {
//   return auth.getUser()
// }).middleware('auth')

Route.group(() => {
  
  use("./Routes/Support/categories")
  use("./Routes/Support/views")
  use("./Routes/Support/roles")
  use("./Routes/Admin/user")
  
}).prefix('/api/v1/').middleware('auth')

Route.get('/stream', ({ source }) => {
  // send a server-sent events comment
  // console.log(source)
  source.send(true);
}).middleware([
  'eventsource',
  'auth'
]);

Route.group(() => {
  
  use("./Routes/Auth/auth")
}).prefix('/api/v1/')

